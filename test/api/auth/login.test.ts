import db from '../../../src/db/database';
import supertest from 'supertest';
import app from '../../../src/app';
import { LoginResponse } from '../../../src/api/auth/login';
import UserRepository from '../../../src/db/repository/UserRepository';
import { ValidationResponse } from '../../../src/types';

describe('POST /api/auth/login', () => {

  const thisDB = db('TEST');
  const user = {
    usernameOrEmail: process.env.DEV_USERNAME,
    password: process.env.DEV_PW
  };

  beforeAll(async () => {
    await thisDB.sync({ force: true });
    await UserRepository.create(process.env.DEV_USERNAME, process.env.DEV_EMAIL, process.env.DEV_PW);
  });

  it('should return a validation error when not providing a username/email in the request', async () => {
    const response = await supertest(app)
      .post('/api/auth/login')
      .send({
        password: process.env.DEV_PW
      })
      .set('Content-Type', 'application/json')
      .expect(400);

    expect((response.body as ValidationResponse).errors).toBeDefined();
    expect((response.body as ValidationResponse).errors).toHaveLength(1);
  });

  it('should return a validation error when not providing a password in the request', async () => {
    const response = await supertest(app)
      .post('/api/auth/login')
      .send({
        usernameOrEmail: process.env.DEV_USERNAME,
      })
      .set('Content-Type', 'application/json')
      .expect(400);

    expect((response.body as ValidationResponse).errors).toBeDefined();
    expect((response.body as ValidationResponse).errors).toHaveLength(1);
  });

  it('should return a token when using the right username and password', async () => {
    const response = await supertest(app)
      .post('/api/auth/login')
      .send(user)
      .set('Content-Type', 'application/json')
      .expect(200);

    expect((response.body as LoginResponse).token).toBeDefined();
  });

  it('should return a token when using the right email and password', async () => {
    const response = await supertest(app)
      .post('/api/auth/login')
      .send({ usernameOrEmail: process.env.DEV_EMAIL, password: process.env.DEV_PW })
      .set('Content-Type', 'application/json')
      .expect(200);

    expect((response.body as LoginResponse).token).toBeDefined();
  });

  it('should return a 403 when using the wrong username', async () => {
    await supertest(app)
      .post('/api/auth/login')
      .send({ usernameOrEmail: 'wrong', password: 'wrong' })
      .set('Content-Type', 'application/json')
      .expect(403);
  });

  it('should return a 403 when using the correct username but the wrong password', async () => {
    await supertest(app)
      .post('/api/auth/login')
      .send({ usernameOrEmail: process.env.DEV_USERNAME, password: 'wrong' })
      .set('Content-Type', 'application/json')
      .expect(403);
  });

  // After all tersts have finished, close the DB connection
  afterAll(async () => {
    await thisDB.close();
  });
});
