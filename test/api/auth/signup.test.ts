import db from '../../../src/db/database';
import supertest from 'supertest';
import app from '../../../src/app';
import { Error } from '../../../src/api/auth/signup';
import UserRepository from '../../../src/db/repository/UserRepository';
import { ValidationResponse } from '../../../src/types';
import { UserResponse } from '../../../src/api/user';

describe('POST /api/auth/signup', () => {

  const thisDB = db('TEST');
  const name = 'someUsername';
  const pw = '123456';
  const email = 'email@gmail.com';

  beforeAll(async () => {
    await thisDB.sync({ force: true });
    await UserRepository.create(process.env.DEV_USERNAME, process.env.DEV_EMAIL, process.env.DEV_PW);
  });

  it('should return a validation error when not providing a username in the request', async () => {
    const response = await supertest(app)
      .post('/api/auth/signup')
      .send({
        password: pw,
        email: email
      })
      .set('Content-Type', 'application/json')
      .expect(400);

    expect((response.body as ValidationResponse).errors).toBeDefined();
    expect((response.body as ValidationResponse).errors).toHaveLength(1);
  });

  it('should return a validation error when not providing an email in the request', async () => {
    const response = await supertest(app)
      .post('/api/auth/signup')
      .send({
        username: process.env.DEV_USERNAME,
        password: pw,
      })
      .set('Content-Type', 'application/json')
      .expect(400);

    expect((response.body as ValidationResponse).errors).toBeDefined();
    expect((response.body as ValidationResponse).errors).toHaveLength(1);
  });

  it('should return a validation error when not providing a password in the request', async () => {
    const response = await supertest(app)
      .post('/api/auth/signup')
      .send({
        username: process.env.DEV_USERNAME,
        email: email,
      })
      .set('Content-Type', 'application/json')
      .expect(400);

    expect((response.body as ValidationResponse).errors).toBeDefined();
    expect((response.body as ValidationResponse).errors).toHaveLength(1);
  });

  it('should return a validation error when the email is not an email in the request', async () => {
    const response = await supertest(app)
      .post('/api/auth/signup')
      .send({
        username: process.env.DEV_USERNAME,
        email: 'notAnEmail',
        password: pw,
      })
      .set('Content-Type', 'application/json')
      .expect(400);

    expect((response.body as ValidationResponse).errors).toBeDefined();
    expect((response.body as ValidationResponse).errors).toHaveLength(1);
  });

  it('should return a validation error when the password is shorter than 6 characters in the request', async () => {
    const response = await supertest(app)
      .post('/api/auth/signup')
      .send({
        username: name,
        email: email,
        password: '123',
      })
      .set('Content-Type', 'application/json')
      .expect(400);

    expect((response.body as ValidationResponse).errors).toBeDefined();
    expect((response.body as ValidationResponse).errors).toHaveLength(1);
  });

  it('should return an error if the username already exists', async () => {
    const response = await supertest(app)
      .post('/api/auth/signup')
      .send({
        username: process.env.DEV_USERNAME,
        password: pw,
        email: email
      })
      .set('Content-Type', 'application/json')
      .expect(400);

    expect((response.body as Error).message).toBeDefined();
  });

  it('should return an error if the email already exists', async () => {
    const response = await supertest(app)
      .post('/api/auth/signup')
      .send({
        username: name,
        password: pw,
        email: process.env.DEV_EMAIL
      })
      .set('Content-Type', 'application/json')
      .expect(400);

    expect((response.body as Error).message).toBeDefined();
  });

  it('should create a user if username and email dont exist yet', async () => {
    const response = await supertest(app)
      .post('/api/auth/signup')
      .send({
        username: name,
        password: pw,
        email: email
      })
      .set('Content-Type', 'application/json')
      .expect(200);

    expect((response.body as UserResponse).username).toEqual(name);
  });

  // After all tersts have finished, close the DB connection
  afterAll(async () => {
    await thisDB.close();
  });
});
