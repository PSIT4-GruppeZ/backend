import Authentication from '../../src/service/Authentication';
import db from '../../src/db/database';
import supertest from 'supertest';
import app from '../../src/app';
import UserRepository from '../../src/db/repository/UserRepository';
import { ValidationResponse } from '../../src/types';
import User from '../../src/db/models/User';
import { LoyalityCardResponse } from '../../src/api/loyalitycard';

describe('/api/loyalitycard', () => {
  const thisDB = db('TEST');

  let token: string;
  let token2: string;
  let uuid: string;
  let user2id: string;
  let user3id: string;

  beforeAll(async () => {
    await thisDB.sync({ force: true });
    await UserRepository.create(process.env.DEV_USERNAME, process.env.DEV_EMAIL, process.env.DEV_PW);
    user2id = (
      await UserRepository.create('2' + process.env.DEV_USERNAME, '2' + process.env.DEV_EMAIL, process.env.DEV_PW)
    ).id;
    user3id = (
      await UserRepository.create('3' + process.env.DEV_USERNAME, '3' + process.env.DEV_EMAIL, process.env.DEV_PW)
    ).id;
    const auth = await Authentication.authenticate(process.env.DEV_USERNAME, process.env.DEV_PW);
    token = auth ? auth.jwt : '';
    const auth2 = await Authentication.authenticate('3' + process.env.DEV_USERNAME, process.env.DEV_PW);
    token2 = auth2 ? auth2.jwt : '';
  });

  it('should be able to create a loyality card when using a valid access token', async () => {
    const loyalityCardName = 'test';
    const barcode = '2501063761689';
    const response = await supertest(app)
      .post('/api/loyalitycard')
      .set('authorization', `Bearer ${token}`)
      .send({ name: loyalityCardName, barcode: barcode })
      .expect(200);

    const res = response.body as LoyalityCardResponse;
    uuid = res.id;
    expect(res.name).toBe(loyalityCardName);
    expect(res.barcode).toBe(barcode);
  });

  it('should not be able to create a loyality card if barcode length is less than 13', async () => {
    const loyalityCardName = 'to short';
    const barcode = '123456';
    await supertest(app)
      .post('/api/loyalitycard')
      .set('authorization', `Bearer ${token}`)
      .send({ name: loyalityCardName, barcode: barcode })
      .expect(400);
  });

  it('should not be able to create a loyality card if barcode length is more than 13', async () => {
    const loyalityCardName = 'to long';
    const barcode = '123456789123456';
    await supertest(app)
      .post('/api/loyalitycard')
      .set('authorization', `Bearer ${token}`)
      .send({ name: loyalityCardName, barcode: barcode })
      .expect(400);
  });

  it('should not be able to create a loyality card if barcode is incorect EAN-13 format', async () => {
    const loyalityCardName = 'to long';
    const barcode = '1234567890123';
    await supertest(app)
      .post('/api/loyalitycard')
      .set('authorization', `Bearer ${token}`)
      .send({ name: loyalityCardName, barcode: barcode })
      .expect(400);
  });

  it('should return a loyality card when using a valid access token', async () => {
    const loyalityCardName = 'test';
    const response = await supertest(app)
      .get('/api/loyalitycard/' + uuid)
      .set('authorization', `Bearer ${token}`)
      .expect(200);

    expect((response.body as LoyalityCardResponse).name).toBe(loyalityCardName);
  });

  it('should return a list of loyality cards when using a valid access token', async () => {
    const response = await supertest(app).get('/api/loyalitycard').set('authorization', `Bearer ${token}`).expect(200);

    expect(response.body as LoyalityCardResponse[]).toHaveLength(1);
  });

  it('should only return a list of loyality cards of the correct user', async () => {
    const response = await supertest(app).get('/api/loyalitycard').set('authorization', `Bearer ${token2}`).expect(200);

    expect(response.body as LoyalityCardResponse[]).toHaveLength(0);
  });

  it('should be able to update a loyality card when using a valid access token', async () => {
    const loyalityCardName = 'test2';
    const barcode = '2099555532317';
    const response = await supertest(app)
      .put('/api/loyalitycard/' + uuid)
      .set('authorization', `Bearer ${token}`)
      .send({ name: loyalityCardName, barcode: barcode })
      .expect(200);

    expect((response.body as LoyalityCardResponse).name).toBe(loyalityCardName);
    expect((response.body as LoyalityCardResponse).barcode).toBe(barcode);
  });

  it('should be able to delete a loyality card when using a valid access token', async () => {
    await supertest(app)
      .delete('/api/loyalitycard/' + uuid)
      .set('authorization', `Bearer ${token}`)
      .expect(200);
  });

  it('should return a 403 when not using a valid access token', async () => {
    await supertest(app).get('/api/loyalitycard').set('authorization', 'Bearer thisTokenIsInvalid').expect(403);
  });

  it('should return a 403 when not using a valid formatted access token', async () => {
    await supertest(app).get('/api/loyalitycard').set('authorization', 'thisTokenIsInvalid').expect(403);
  });

  it('should return a 400 when not providing an authorization header', async () => {
    const response = await supertest(app).get('/api/loyalitycard').expect(400);

    expect((response.body as ValidationResponse).errors).toBeDefined();
    expect((response.body as ValidationResponse).errors).toHaveLength(1);
  });

  it('should return a 404 on read if loyality card does not exist', async () => {
    await supertest(app)
      .get('/api/loyalitycard/uuuuuuuu-uuuu-uuii-iiii-dddddddddddd')
      .set('authorization', `Bearer ${token}`)
      .expect(404);
  });

  it('should return a 404 on delete if loyality card does not exist', async () => {
    await supertest(app)
      .delete('/api/loyalitycard/uuuuuuuu-uuuu-uuii-iiii-dddddddddddd')
      .set('authorization', `Bearer ${token}`)
      .expect(404);
  });

  it('should return a 404 on update if loyality card does not exist', async () => {
    await supertest(app)
      .put('/api/loyalitycard/uuuuuuuu-uuuu-uuii-iiii-dddddddddddd')
      .set('authorization', `Bearer ${token}`)
      .send({ name: 'cart', barcode: '2099555532317' })
      .expect(404);
  });

  // After all tersts have finished, close the DB connection
  afterAll(async () => {
    await thisDB.close();
  });
});
