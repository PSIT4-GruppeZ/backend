import Authentication from '../../src/service/Authentication';
import db from '../../src/db/database';
import supertest from 'supertest';
import app from '../../src/app';
import UserRepository from '../../src/db/repository/UserRepository';
import { ValidationResponse } from '../../src/types';
import { ItemResponse } from '../../src/api/item';
import CategoryRepository from '../../src/db/repository/CategoryRepository';

describe('/api/item', () => {

  const thisDB = db('TEST');

  let token: string;
  let uuid: string;
  const itemName = 'testitem';
  const catName = 'testcategory';
  let catId: string;

  beforeAll(async () => {
    await thisDB.sync({ force: true });
    await UserRepository.create(process.env.DEV_USERNAME, process.env.DEV_EMAIL, process.env.DEV_PW);
    const auth = await Authentication.authenticate(process.env.DEV_USERNAME, process.env.DEV_PW);
    catId = (await CategoryRepository.create(catName)).id;
    token = auth ? auth.jwt : '';
  });

  it('should be able to create an item when using a valid access token', async () => {
    const response = await supertest(app)
      .post('/api/item')
      .set('authorization', `Bearer ${token}`)
      .send({ name: itemName, categoryId: catId })
      .expect(200);

    const res = (response.body as ItemResponse);
    uuid = res.id;
    expect(res.name).toBe(itemName);
  });

  it('should return a list of items when using a valid access token', async () => {
    const response = await supertest(app)
      .get('/api/item')
      .set('authorization', `Bearer ${token}`)
      .expect(200);

    expect(response.body as ItemResponse[]).toHaveLength(1);
  });

  it('should return an item on uuid query when using a valid access token', async () => {
    const response = await supertest(app)
      .get('/api/item/' + uuid)
      .set('authorization', `Bearer ${token}`)
      .expect(200);

    expect((response.body as ItemResponse).name).toBe(itemName);
  });

  it('should return an item on name query when using a valid access token', async () => {
    const response = await supertest(app)
      .get('/api/item/' + itemName)
      .set('authorization', `Bearer ${token}`)
      .expect(200);

    expect((response.body as ItemResponse).name).toBe(itemName);
  });

  it('should be able to update an item when using a valid access token', async () => {
    const itemName2 = 'test2';
    const response = await supertest(app)
      .put('/api/item/' + uuid)
      .set('authorization', `Bearer ${token}`)
      .send({ name: itemName2 })
      .expect(200);

    expect((response.body as ItemResponse).name).toBe(itemName2);
  });

  it('should be able to delete an item when using a valid access token', async () => {
    await supertest(app)
      .delete('/api/item/' + uuid)
      .set('authorization', `Bearer ${token}`)
      .expect(200);
  });

  it('should return a 403 when not using a valid access token', async () => {
    await supertest(app)
      .get('/api/item')
      .set('authorization', 'Bearer thisTokenIsInvalid')
      .expect(403);
  });

  it('should return a 403 when not using a valid formatted access token', async () => {
    await supertest(app)
      .get('/api/item')
      .set('authorization', 'thisTokenIsInvalid')
      .expect(403);
  });

  it('should return a 400 when not providing a category id', async () => {
    const response = await supertest(app)
      .post('/api/item')
      .set('authorization', `Bearer ${token}`)
      .send({ name: itemName })
      .expect(400);

      expect((response.body as ValidationResponse).errors).toBeDefined();
      expect((response.body as ValidationResponse).errors).toHaveLength(1);
  });

  it('should return a 400 when not providing an invalid category id', async () => {
    await supertest(app)
      .post('/api/item')
      .set('authorization', `Bearer ${token}`)
      .send({ name: itemName, categoryId: 'asdf' })
      .expect(400);
  });

  it('should return a 400 when not providing an authorization header', async () => {
    const response = await supertest(app)
      .get('/api/item')
      .expect(400);

    expect((response.body as ValidationResponse).errors).toBeDefined();
    expect((response.body as ValidationResponse).errors).toHaveLength(1);
  });

  it('should return a 404 on read if item does not exist', async () => {
    await supertest(app)
      .get('/api/item/uuuuuuuu-uuuu-uuii-iiii-dddddddddddd')
      .set('authorization', `Bearer ${token}`)
      .expect(404);
  });

  it('should return a 404 on delete if item does not exist', async () => {
    await supertest(app)
      .delete('/api/item/uuuuuuuu-uuuu-uuii-iiii-dddddddddddd')
      .set('authorization', `Bearer ${token}`)
      .expect(404);
  });

  it('should return a 404 on update if item does not exist', async () => {
    await supertest(app)
      .put('/api/item/uuuuuuuu-uuuu-uuii-iiii-dddddddddddd')
      .set('authorization', `Bearer ${token}`)
      .send({ name: 'item' })
      .expect(404);
  });

  // After all tersts have finished, close the DB connection
  afterAll(async () => {
    await thisDB.close();
  });
});
