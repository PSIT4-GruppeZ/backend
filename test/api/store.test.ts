import Authentication from '../../src/service/Authentication';
import db from '../../src/db/database';
import { StoreResponse } from '../../src/api/store';
import supertest from 'supertest';
import app from '../../src/app';
import UserRepository from '../../src/db/repository/UserRepository';
import { ValidationResponse } from '../../src/types';

describe('/api/store', () => {

  const thisDB = db('TEST');

  let token: string;
  let uuid: string;

  beforeAll(async () => {
    await thisDB.sync({ force: true });
    await UserRepository.create(process.env.DEV_USERNAME, process.env.DEV_EMAIL, process.env.DEV_PW);
    const auth = await Authentication.authenticate(process.env.DEV_USERNAME, process.env.DEV_PW);
    token = auth ? auth.jwt : '';
  });

  it('should be able to create a store when using a valid access token', async () => {
    const storeName = 'test';
    const response = await supertest(app)
      .post('/api/store')
      .set('authorization', `Bearer ${token}`)
      .send({ name: storeName })
      .expect(200);

    const res = (response.body as StoreResponse);
    uuid = res.id;
    expect(res.name).toBe(storeName);
  });

  it('should return a store when using a valid access token', async () => {
    const storeName = 'test';
    const response = await supertest(app)
      .get('/api/store/' + uuid)
      .set('authorization', `Bearer ${token}`)
      .expect(200);

    expect((response.body as StoreResponse).name).toBe(storeName);
  });

  it('should return a list of stores when using a valid access token', async () => {
    const response = await supertest(app)
      .get('/api/store')
      .set('authorization', `Bearer ${token}`)
      .expect(200);

    expect(response.body as StoreResponse[]).toHaveLength(1);
  });

  it('should be able to update a store when using a valid access token', async () => {
    const storeName = 'test2';
    const response = await supertest(app)
      .put('/api/store/' + uuid)
      .set('authorization', `Bearer ${token}`)
      .send({ name: storeName })
      .expect(200);

    expect((response.body as StoreResponse).name).toBe(storeName);
  });

  it('should be able to delete a store when using a valid access token', async () => {
    await supertest(app)
      .delete('/api/store/' + uuid)
      .set('authorization', `Bearer ${token}`)
      .expect(200);
  });

  it('should return a 403 when not using a valid access token', async () => {
    await supertest(app)
      .get('/api/store')
      .set('authorization', 'Bearer thisTokenIsInvalid')
      .expect(403);
  });

  it('should return a 403 when not using a valid formatted access token', async () => {
    await supertest(app)
      .get('/api/store')
      .set('authorization', 'thisTokenIsInvalid')
      .expect(403);
  });

  it('should return a 400 when not providing an authorization header', async () => {
    const response = await supertest(app)
      .get('/api/store')
      .expect(400);

    expect((response.body as ValidationResponse).errors).toBeDefined();
    expect((response.body as ValidationResponse).errors).toHaveLength(1);
  });

  it('should return a 404 on read if store does not exist', async () => {
    await supertest(app)
      .get('/api/store/uuuuuuuu-uuuu-uuii-iiii-dddddddddddd')
      .set('authorization', `Bearer ${token}`)
      .expect(404);
  });

  it('should return a 404 on delete if store does not exist', async () => {
    await supertest(app)
      .delete('/api/store/uuuuuuuu-uuuu-uuii-iiii-dddddddddddd')
      .set('authorization', `Bearer ${token}`)
      .expect(404);
  });

  it('should return a 404 on update if store does not exist', async () => {
    await supertest(app)
      .put('/api/store/uuuuuuuu-uuuu-uuii-iiii-dddddddddddd')
      .set('authorization', `Bearer ${token}`)
      .send({ name: 'store' })
      .expect(404);
  });

  // After all tersts have finished, close the DB connection
  afterAll(async () => {
    await thisDB.close();
  });
});
