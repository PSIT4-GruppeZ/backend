import supertest from 'supertest';
import { CategoryResponse } from '../../../src/api/category';
import app from '../../../src/app';
import db from '../../../src/db/database';
import UserRepository from '../../../src/db/repository/UserRepository';
import Authentication from '../../../src/service/Authentication';
import { ValidationResponse } from '../../../src/types';

describe('/api/category', () => {
  const thisDB = db('TEST');
  let token: string;
  let uuid: string;

  beforeAll(async () => {
    await thisDB.sync({ force: true });
    await UserRepository.create(process.env.DEV_USERNAME, process.env.DEV_EMAIL, process.env.DEV_PW);
    const auth = await Authentication.authenticate(process.env.DEV_USERNAME, process.env.DEV_PW);
    token = auth ? auth.jwt : '';
  });

  it('should be able to create a category when using a valid access token', async () => {
    const name = 'category';
    const response = await supertest(app)
      .post('/api/category')
      .set('authorization', `Bearer ${token}`)
      .send({ name: name })
      .expect(200);

    const res = response.body as CategoryResponse;
    uuid = res.id;
    expect(res.name).toBe(name);
  });

  it('should return a list of category when using a valid access token', async () => {
    const response = await supertest(app).get('/api/category').set('authorization', `Bearer ${token}`).expect(200);
    expect(response.body as CategoryResponse[]).toHaveLength(1);
  });

  it('should be able to update a category when using a valid access token', async () => {
    const category = 'category2';
    const response = await supertest(app)
      .put('/api/category/' + uuid)
      .set('authorization', `Bearer ${token}`)
      .send({ name: category })
      .expect(200);

    expect((response.body as CategoryResponse).name).toBe(category);
  });

  it('should be able to remove a categoryitem when using a valid access token', async () => {
    await supertest(app)
      .delete('/api/category/' + uuid)
      .set('authorization', `Bearer ${token}`)
      .expect(200);

    const listResponse = await supertest(app).get('/api/category').set('authorization', `Bearer ${token}`).expect(200);
    expect(listResponse.body as CategoryResponse[]).toHaveLength(0);
  });

  it('should return a 403 when not using a valid access token', async () => {
    await supertest(app).get('/api/category').set('authorization', 'Bearer thisTokenIsInvalid').expect(403);
  });

  it('should return a 403 when not using a valid formatted access token', async () => {
    await supertest(app).get('/api/category').set('authorization', 'thisTokenIsInvalid').expect(403);
  });

  it('should return a 400 when not providing an authorization header', async () => {
    const response = await supertest(app).get('/api/category').expect(400);

    expect((response.body as ValidationResponse).errors).toBeDefined();
    expect((response.body as ValidationResponse).errors).toHaveLength(1);
  });

  it('should return a 404 on delete if category does not exist', async () => {
    await supertest(app)
      .delete('/api/category/uuuuuuuu-uuuu-uuii-iiii-dddddddddddd')
      .set('authorization', `Bearer ${token}`)
      .expect(404);
  });

  it('should return a 404 on update if category does not exist', async () => {
    await supertest(app)
      .put('/api/category/uuuuuuuu-uuuu-uuii-iiii-dddddddddddd')
      .set('authorization', `Bearer ${token}`)
      .send({ name: 'category' })
      .expect(404);
  });

  // After all tersts have finished, close the DB connection
  afterAll(async () => {
    await thisDB.close();
  });
});
