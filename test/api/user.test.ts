import Authentication from '../../src/service/Authentication';
import db from '../../src/db/database';
import supertest from 'supertest';
import app from '../../src/app';
import UserRepository from '../../src/db/repository/UserRepository';
import { ValidationResponse } from '../../src/types';
import { UserResponse } from '../../src/api/user';

describe('/api/user', () => {

  const thisDB = db('TEST');

  let token: string;
  const username = process.env.DEV_USERNAME;
  const email = process.env.DEV_EMAIL;

  beforeAll(async () => {
    await thisDB.sync({ force: true });
    await UserRepository.create(process.env.DEV_USERNAME, process.env.DEV_EMAIL, process.env.DEV_PW);
    const auth = await Authentication.authenticate(process.env.DEV_USERNAME, process.env.DEV_PW);
    token = auth ? auth.jwt : '';
  });

  it('should return an user on username query when using a valid access token', async () => {
    const response = await supertest(app)
      .get('/api/user/' + username)
      .set('authorization', `Bearer ${token}`)
      .expect(200);

    expect((response.body as UserResponse).username).toBe(username);
  });

  it('should return an user on email query when using a valid access token', async () => {
    const response = await supertest(app)
      .get('/api/user/' + email)
      .set('authorization', `Bearer ${token}`)
      .expect(200);

    expect((response.body as UserResponse).email).toBe(email);
  });

  it('should return a list of users when using a valid access token', async () => {
    const response = await supertest(app)
      .get('/api/user')
      .set('authorization', `Bearer ${token}`)
      .expect(200);

    expect(response.body as UserResponse[]).toHaveLength(1);
  });

  it('should return a 403 when not using a valid access token', async () => {
    await supertest(app)
      .get('/api/user')
      .set('authorization', 'Bearer thisTokenIsInvalid')
      .expect(403);
  });

  it('should return a 403 when not using a valid formatted access token', async () => {
    await supertest(app)
      .get('/api/user')
      .set('authorization', 'thisTokenIsInvalid')
      .expect(403);
  });

  it('should return a 400 when not providing an authorization header', async () => {
    const response = await supertest(app)
      .get('/api/user')
      .expect(400);

    expect((response.body as ValidationResponse).errors).toBeDefined();
    expect((response.body as ValidationResponse).errors).toHaveLength(1);
  });

  it('should return a 404 on read if user does not exist', async () => {
    await supertest(app)
      .get('/api/user/invalidUser')
      .set('authorization', `Bearer ${token}`)
      .expect(404);
  });

  // After all tersts have finished, close the DB connection
  afterAll(async () => {
    await thisDB.close();
  });
});
