import Authentication from '../../src/service/Authentication';
import db from '../../src/db/database';
import { CartResponse } from '../../src/api/cart';
import supertest from 'supertest';
import app from '../../src/app';
import UserRepository from '../../src/db/repository/UserRepository';
import { ValidationResponse } from '../../src/types';
import ItemRepository from '../../src/db/repository/ItemRepository';
import CategoryRepository from '../../src/db/repository/CategoryRepository';

describe('/api/cart', () => {

  const thisDB = db('TEST');

  let token: string;
  let uuid: string;
  let itemId: string;
  let catId: string;
  const itemName = 'testitem';
  const catName = 'testcategory';
  let user2id: string;
  let user3id: string;

  beforeAll(async () => {
    await thisDB.sync({ force: true });
    await UserRepository.create(process.env.DEV_USERNAME, process.env.DEV_EMAIL, process.env.DEV_PW);
    user2id = (await UserRepository.create('2'+process.env.DEV_USERNAME, '2'+process.env.DEV_EMAIL, process.env.DEV_PW)).id;
    user3id = (await UserRepository.create('3'+process.env.DEV_USERNAME, '3'+process.env.DEV_EMAIL, process.env.DEV_PW)).id;
    catId = (await CategoryRepository.create(catName)).id;
    itemId = (await ItemRepository.create(itemName, catId)).id;
    const auth = await Authentication.authenticate(process.env.DEV_USERNAME, process.env.DEV_PW);
    token = auth ? auth.jwt : '';
  });

  it('should be able to create a cart when using a valid access token', async () => {
    const cartName = 'test';
    const response = await supertest(app)
      .post('/api/cart')
      .set('authorization', `Bearer ${token}`)
      .send({ name: cartName })
      .expect(200);

    const res = (response.body as CartResponse);
    uuid = res.id;
    expect(res.name).toBe(cartName);
  });

  it('should return a cart when using a valid access token', async () => {
    const response = await supertest(app)
      .get('/api/cart/' + uuid)
      .set('authorization', `Bearer ${token}`)
      .expect(200);

    expect((response.body as CartResponse).items).toHaveLength(0);
  });

  it('should return a list of carts when using a valid access token', async () => {
    const response = await supertest(app)
      .get('/api/cart')
      .set('authorization', `Bearer ${token}`)
      .expect(200);

    expect(response.body as CartResponse[]).toHaveLength(1);
  });

  it('should be able to update a cart when using a valid access token', async () => {
    const cartName = 'test2';
    const response = await supertest(app)
      .put('/api/cart/' + uuid)
      .set('authorization', `Bearer ${token}`)
      .send({ name: cartName })
      .expect(200);

    expect((response.body as CartResponse).name).toBe(cartName);
  });

  it('should be able to add a cartitem when using a valid access token', async () => {
    const response = await supertest(app)
      .post('/api/cart/' + uuid + '/items')
      .set('authorization', `Bearer ${token}`)
      .send({ id: itemId })
      .expect(200);

    const res = (response.body as CartResponse);
    expect(res.items).toHaveLength(1);
    expect(res.items[0].name).toBe(itemName);
  });

  it('should be able to share a cart with an user when using a valid access token', async () => {
    const response = await supertest(app)
      .post('/api/cart/' + uuid + '/share')
      .set('authorization', `Bearer ${token}`)
      .send({ userid: user2id })
      .expect(200);

    expect((response.body as CartResponse).users).toHaveLength(2);
  });

  it('should return a 404 on share cart if user does not exist', async () => {
    await supertest(app)
      .post('/api/cart/' + uuid + '/share')
      .set('authorization', `Bearer ${token}`)
      .send({ userid: 'uuuuuuuu-uuuu-uuii-iiii-dddddddddddd' })
      .expect(404);
  });

  it('should be able to update a cartitem when using a valid access token', async () => {
    const response = await supertest(app)
      .put('/api/cart/' + uuid + '/items/' + itemId)
      .set('authorization', `Bearer ${token}`)
      .send({ checked: true })
      .expect(200);

    const res = (response.body as CartResponse);
    expect(res.items).toHaveLength(1);
    expect(res.items[0].checked).toBe(true);
  });

  it('should be able to remove a cartitem when using a valid access token', async () => {
    const response = await supertest(app)
      .delete('/api/cart/' + uuid + '/items/' + itemId)
      .set('authorization', `Bearer ${token}`)
      .expect(200);

    expect((response.body as CartResponse).items).toHaveLength(0);
  });

  it('should be able to unshare a cart when using a valid access token', async () => {
    const response = await supertest(app)
      .delete('/api/cart/' + uuid + '/share/' + user2id)
      .set('authorization', `Bearer ${token}`)
      .expect(200);

    expect((response.body as CartResponse).users).toHaveLength(1);
  });

  it('should return a 404 on unshare cart if user does not exist', async () => {
    await supertest(app)
      .delete('/api/cart/' + uuid + '/share/uuuuuuuu-uuuu-uuii-iiii-dddddddddddd')
      .set('authorization', `Bearer ${token}`)
      .expect(404);
  });

  it('should return a 404 on unshare cart if user does not have this cart', async () => {
    await supertest(app)
      .delete('/api/cart/' + uuid + '/share/' + user3id)
      .set('authorization', `Bearer ${token}`)
      .expect(404);
  });

  it('should return a 404 on updateItem if cartitem does not exist', async () => {
    await supertest(app)
      .put('/api/cart/' + uuid + '/items/uuuuuuuu-uuuu-uuii-iiii-dddddddddddd')
      .set('authorization', `Bearer ${token}`)
      .send({ checked: true })
      .expect(404);
  });

  it('should be able to delete a cart when using a valid access token', async () => {
    await supertest(app)
      .delete('/api/cart/' + uuid)
      .set('authorization', `Bearer ${token}`)
      .expect(200);
  });

  it('should return a 403 when not using a valid access token', async () => {
    await supertest(app)
      .get('/api/cart')
      .set('authorization', 'Bearer thisTokenIsInvalid')
      .expect(403);
  });

  it('should return a 403 when not using a valid formatted access token', async () => {
    await supertest(app)
      .get('/api/cart')
      .set('authorization', 'thisTokenIsInvalid')
      .expect(403);
  });

  it('should return a 400 when not providing an authorization header', async () => {
    const response = await supertest(app)
      .get('/api/cart')
      .expect(400);

    expect((response.body as ValidationResponse).errors).toBeDefined();
    expect((response.body as ValidationResponse).errors).toHaveLength(1);
  });

  it('should return a 404 on read if cart does not exist', async () => {
    await supertest(app)
      .get('/api/cart/uuuuuuuu-uuuu-uuii-iiii-dddddddddddd')
      .set('authorization', `Bearer ${token}`)
      .expect(404);
  });

  it('should return a 404 on delete if cart does not exist', async () => {
    await supertest(app)
      .delete('/api/cart/uuuuuuuu-uuuu-uuii-iiii-dddddddddddd')
      .set('authorization', `Bearer ${token}`)
      .expect(404);
  });

  it('should return a 404 on update if cart does not exist', async () => {
    await supertest(app)
      .put('/api/cart/uuuuuuuu-uuuu-uuii-iiii-dddddddddddd')
      .set('authorization', `Bearer ${token}`)
      .send({ name: 'cart' })
      .expect(404);
  });

  it('should return a 404 on addItem if cart does not exist', async () => {
    await supertest(app)
      .post('/api/cart/uuuuuuuu-uuuu-uuii-iiii-dddddddddddd/items')
      .set('authorization', `Bearer ${token}`)
      .send({ id: 'uuuuuuuu-uuuu-uuii-iiii-dddddddddddd' })
      .expect(404);
  });

  it('should return a 404 on share cart if cart does not exist', async () => {
    await supertest(app)
      .post('/api/cart/uuuuuuuu-uuuu-uuii-iiii-dddddddddddd/share')
      .set('authorization', `Bearer ${token}`)
      .send({ userid: 'uuuuuuuu-uuuu-uuii-iiii-dddddddddddd' })
      .expect(404);
  });

  it('should return a 404 on deleteItem if cart does not exist', async () => {
    await supertest(app)
      .delete('/api/cart/uuuuuuuu-uuuu-uuii-iiii-dddddddddddd/items/uuuuuuuu-uuuu-uuii-iiii-dddddddddddd')
      .set('authorization', `Bearer ${token}`)
      .expect(404);
  });

  it('should return a 404 on unshare cart if cart does not exist', async () => {
    await supertest(app)
      .delete('/api/cart/uuuuuuuu-uuuu-uuii-iiii-dddddddddddd/share/uuuuuuuu-uuuu-uuii-iiii-dddddddddddd')
      .set('authorization', `Bearer ${token}`)
      .expect(404);
  });

  it('should return a 404 on updateItem if cart does not exist', async () => {
    await supertest(app)
      .put('/api/cart/uuuuuuuu-uuuu-uuii-iiii-dddddddddddd/items/uuuuuuuu-uuuu-uuii-iiii-dddddddddddd')
      .set('authorization', `Bearer ${token}`)
      .send({ checked: true })
      .expect(404);
  });

  // After all tersts have finished, close the DB connection
  afterAll(async () => {
    await thisDB.close();
  });
});
