import { Category } from '../models/Category';

export default class CategoryRepository {
  static async findById(id: string): Promise<Category> {
    return await Category.findOne({ where: { id } });
  }

  static async findAll(): Promise<Category[]> {
    return await Category.findAll();
  }

  static async create(name: string): Promise<Category> {
    let category: Category;
    try {
      category = await Category.create({ name });
    } catch (e) {
      console.log(`Category "${name}" already created`);
    }
    return category;
  }

  static async update(id: string, name: string): Promise<Category> {
    await Category.update({ name }, { where: { id } });
    return await this.findById(id);
  }

  static async deleteById(id: string): Promise<number> {
    return await Category.destroy({ where: { id } });
  }
}
