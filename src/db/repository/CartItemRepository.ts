import { CartItem } from '../models/CartItem';

export default class CartItemRepository {
  static async find(cartId: string, itemId: string): Promise<CartItem> {
    return await CartItem.findOne({ where: { CartId: cartId, ItemId: itemId } });
  }

  static async create(cartId: string, itemId: string): Promise<CartItem> {
    return await CartItem.create({ CartId: cartId, ItemId: itemId });
  }

  static async delete(cartId: string, itemId: string): Promise<number> {
    return await CartItem.destroy({ where: { CartId: cartId, ItemId: itemId } });
  }

  static async update(cartId: string, itemId: string, checked: boolean): Promise<CartItem> {
    await CartItem.update({ checked }, { where: { CartId: cartId, ItemId: itemId } });
    return await this.find(cartId, itemId);
  }
}