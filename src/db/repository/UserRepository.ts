import { Op } from 'sequelize';
import Authentication from '../../service/Authentication';
import User from '../models/User';

export default class UserRepository {
  static async findByUsername(username: string): Promise<User> {
    return await User.findOne({ where: { username: username } });
  }

  static async findByEmail(email: string): Promise<User> {
    return await User.findOne({ where: { email } });
  }

  static async findByUsernameOrEmail(usernameOrEmail: string): Promise<User> {
    return await User.findOne({
      where: {
        [Op.or]: [
          { username: usernameOrEmail },
          { email: usernameOrEmail }
        ]
      }
    });
  }

  static async findById(id: string): Promise<User> {
    return await User.findOne({ where: { id } });
  }

  static async findAll(): Promise<User[]> {
    return await User.findAll();
  }

  static async existsByUsername(username: string): Promise<boolean> {
    const user = await this.findByUsername(username);
    return user !== null && user instanceof User;
  }

  static async existsByEmail(email: string): Promise<boolean> {
    const user = await this.findByEmail(email);
    return user !== null && user instanceof User;
  }

  /**
   * @param username 
   * @param email 
   * @param password in clear text
   * @returns a new User
   */
  static async create(username: string, email: string, password: string): Promise<User> {
    let user: User;
    try {
      user = await User.create({
        username,
        email,
        password: Authentication.hashPassword(password),
      });
    } catch (e) {
      console.log(`User "${username}" already created`);
    }
    return user;
  }
}