import { CartUser } from '../models/CartUser';

export default class CartUserRepository {
  static async find(cartId: string, userId: string): Promise<CartUser> {
    return CartUser.findOne({ where: { CartId: cartId, UserId: userId } });
  }

  static async create(cartId: string, userId: string): Promise<CartUser> {
    return await CartUser.create({ CartId: cartId, UserId: userId });
  }

  static async delete(cartId: string, userId: string): Promise<number> {
    return await CartUser.destroy({ where: { CartId: cartId, UserId: userId } });
  }
  
  static async exists(cartId: string, userId: string): Promise<boolean> {
    const cartUser = await this.find(cartId, userId);
    return cartUser !== null && cartUser instanceof CartUser;
  }
}