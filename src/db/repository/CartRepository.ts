import { Cart } from '../models/Cart';
import { Item } from '../models/Item';
import User from '../models/User';
import CartUserRepository from './CartUserRepository';

export default class CartRepository {
  static async findById(id: string, userId: string): Promise<Cart> {
    if (CartUserRepository.exists(id, userId)) {
      return await Cart.findOne({ where: { id }, include: [ Item, User ] });
    }
  }

  static async findAll(userId: string): Promise<Cart[]> {
    return await Cart.findAll({ include: [ Item, { model: User, where: { id: userId }} ] });
  }

  static async deleteById(id: string, userId: string): Promise<number> {
    if (await this.findById(id, userId)) {
      return await Cart.destroy({ where: { id } });
    }
  }

  static async create(name: string, userId: string): Promise<Cart> {
    const cart = await Cart.create({ name });
    await CartUserRepository.create(cart.id, userId);
    return await this.findById(cart.id, userId);
  }

  static async update(id: string, name: string, userId: string): Promise<Cart> {
    if (await this.findById(id, userId)) {
      await Cart.update({ name }, { where: { id } });
    }
    return await this.findById(id, userId);
  }
}