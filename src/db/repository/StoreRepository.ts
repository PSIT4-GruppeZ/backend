import { Category } from '../models/Category';
import { Store } from '../models/Store';
import CategoryRepository from './CategoryRepository';

export default class StoreRepository {
  /**
   * If the store doesn't contain ordering information for all
   * categories, it will be added here. This makes sure no categories
   * are missing for a store.
   *
   * @param store the store to be verified
   */
  private static async ensureCatogories(store: Store, categories: Category[]): Promise<void> {
    let hasChanges = false;
    
    let categoryOrder: string[] = [];

    if (store.categoryOrder == null) {
      hasChanges = true;
    } else {
      categoryOrder = store.categoryOrder;
    }

    // Delete any categories that might've been left over
    categoryOrder = categoryOrder.filter((categoryId) => {
      if (!categories.find((cat) => cat.id == categoryId) != null) {
        return true;
      }
      hasChanges = true;
      return false;
    });

    // Add any missing categories
    categories.forEach((category) => {
      if (!(categoryOrder.includes(category.id))) {
        hasChanges = true;
        categoryOrder.push(category.id);
      }
    });

    if (hasChanges) {
      store.categoryOrder = categoryOrder;
      store.save();
    }
  }

  static async findAll(): Promise<Store[]> {
    // We're not ensuring all categories here
    // The application must use findById when displaying categories
    const categories = await CategoryRepository.findAll();
    const stores = await Store.findAll();
    stores.forEach((store) => this.ensureCatogories(store, categories));
    return stores;
  }

  static async findById(id: string): Promise<Store> {
    const store = await Store.findOne({ where: { id } });
    await this.ensureCatogories(store, await CategoryRepository.findAll());
    return store;
  }

  static async deleteById(id: string): Promise<number> {
    return await Store.destroy({ where: { id } });
  }

  static async create(name: string): Promise<Store> {
    const store = await Store.create({
      name,
    });
    await this.ensureCatogories(store, await CategoryRepository.findAll());
    return store;
  }

  static async update(id: string, name: string): Promise<Store> {
    await Store.update({ name }, { where: { id } });
    return await this.findById(id);
  }

  static async updateCategoryOrder(id: string, categoryOrder: string[]): Promise<Store> {
    await Store.update({ categoryOrder }, {where: {id}});
    return await this.findById(id);
  }
}
