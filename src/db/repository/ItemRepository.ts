import { Category } from '../models/Category';
import { Op } from 'sequelize';
import { Item } from '../models/Item';

export default class ItemRepository {
  static async findById(id: string): Promise<Item> {
    return await Item.findOne({ where: { id }, include: [Category] });
  }

  static async findByIdOrName(query: string): Promise<Item> {
    return await Item.findOne({ where: { [Op.or]: [ { id: query }, { name: query } ] }, include: [Category] });
  }

  static async findAll(): Promise<Item[]> {
    return await Item.findAll({ include: [Category] });
  }

  static async create(name: string, categoryId: string): Promise<Item | undefined> {
    let item: Item;
    try {
      item = await Item.create({ name, CategoryId: categoryId });
      return this.findById(item.id);
    } catch (e) {
      return;
    }
  }

  static async update(id: string, name: string): Promise<Item> {
    await Item.update({ name }, { where: { id } });
    return await this.findById(id);
  }

  static async deleteById(id: string): Promise<number> {
    return await Item.destroy({ where: { id } });
  }
}