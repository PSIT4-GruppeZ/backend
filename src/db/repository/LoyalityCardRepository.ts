import { Op } from 'sequelize';
import { LoyalityCard } from '../models/LoyalityCard';
import User from '../models/User';

export default class LoyalityCardRepository {
  static async findById(id: string, userId: string): Promise<LoyalityCard> {
    return await LoyalityCard.findOne({
      where: { id },
      include: [{ model: User, where: { id: userId } }],
    });
  }

  static async findAll(userId: string): Promise<LoyalityCard[]> {
    return await LoyalityCard.findAll({ where: { UserId: userId } });
  }

  static async create(name: string, barcode: string, userId: string): Promise<LoyalityCard> {
    return await LoyalityCard.create({
      name: name,
      barcode: barcode,
      UserId: userId,
    });
  }

  static async update(id: string, name: string, barcode: string, userId: string): Promise<LoyalityCard> {
    await LoyalityCard.update(
      {
        name,
        barcode,
      },
      { where: { id } },
    );
    return await this.findById(id, userId);
  }

  static async deleteById(id: string, userId: string): Promise<number> {
    return await LoyalityCard.destroy({
      where: {
        [Op.and]: [{ id: id }, { UserId: userId }],
      },
    });
  }
}
