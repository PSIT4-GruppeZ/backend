import { Sequelize } from 'sequelize';
import User from './models/User';
import config from '../config';
import { AppEnv } from '../types';
import { Cart } from './models/Cart';
import { Item } from './models/Item';
import { CartItem } from './models/CartItem';
import { CartUser } from './models/CartUser';
import { Store } from './models/Store';
import { Category } from './models/Category';
import { LoyalityCard } from './models/LoyalityCard';

/**
 * Add your models here
 */
const models = [Cart, CartItem, CartUser, User, Category, Store, Item, LoyalityCard];

const db = (env: AppEnv): Sequelize => {
  const sequelize = new Sequelize(config[env].db);

  models.forEach((model) => {
    model._init(sequelize);
  });

  Cart.belongsToMany(Item, { through: CartItem });
  Item.belongsToMany(Cart, { through: CartItem });
  Cart.belongsToMany(User, { through: CartUser });
  User.belongsToMany(Cart, { through: CartUser });
  LoyalityCard.belongsTo(User, { foreignKey: { allowNull: false } });

  Item.belongsTo(Category, { foreignKey: { allowNull: false } });
  Category.hasMany(Item);

  return sequelize;
};

export default db;
