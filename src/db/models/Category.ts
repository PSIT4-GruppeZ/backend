import { DataTypes, Model, Sequelize } from 'sequelize';
import { v4 as uuidv4 } from 'uuid';
import { CategoryResponse } from '../../api/category';

export class Category extends Model {
  public id!: string;
  public name: string;

  static _init(sequelize: Sequelize): void {
    Category.init(
      {
        id: {
          type: DataTypes.UUID,
          primaryKey: true,
          allowNull: false,
          defaultValue: () => uuidv4(),
        },
        name: {
          type: DataTypes.STRING,
          unique: true,
          allowNull: false,
        },
      },
      {
        sequelize,
        modelName: 'Category',
      },
    );
  }

  getResponse(): CategoryResponse {
    return {
      id: this.id,
      name: this.name,
    };
  }
}
