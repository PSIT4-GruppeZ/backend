import { DataTypes, Model, Sequelize } from 'sequelize';
import { v4 as uuidv4 } from 'uuid';
import { LoyalityCardResponse } from '../../api/loyalitycard';
import User from './User';

export class LoyalityCard extends Model {
  public id!: string;
  public name: string;
  public barcode: string;
  public user: User;

  static _init(sequelize: Sequelize): void {
    LoyalityCard.init(
      {
        id: {
          type: DataTypes.UUID,
          primaryKey: true,
          allowNull: false,
          defaultValue: () => uuidv4(),
        },
        name: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        barcode: {
          type: DataTypes.STRING,
          allowNull: false,
        },
      },
      {
        sequelize,
        modelName: 'LoyalityCard',
      },
    );
  }

  getResponse(): LoyalityCardResponse {
    return {
      id: this.id,
      name: this.name,
      barcode: this.barcode,
    };
  }
}
