import { DataTypes, Model, Sequelize } from 'sequelize';

export class CartItem extends Model {
  public CartId: string;
  public ItemId: string;
  public checked: boolean;

  static _init(sequelize: Sequelize,): void {
    CartItem.init(
      {
        checked: {
          type: DataTypes.BOOLEAN,
          allowNull: false,
          defaultValue: false,
        },
      },
      {
        sequelize,
        modelName: 'CartItem',
      }
    );
  }
}
