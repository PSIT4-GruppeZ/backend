import { Model, Sequelize } from 'sequelize';

export class CartUser extends Model {
  public CartId: string;
  public UserId: string;

  static _init(sequelize: Sequelize,): void {
    CartUser.init(
      {},
      {
        sequelize,
        modelName: 'CartUser',
      }
    );
  }
}