import { DataTypes, Model, Sequelize } from 'sequelize';
import { v4 as uuidv4 } from 'uuid';
import { CartResponse } from '../../api/cart';
import { Item } from './Item';
import { Store } from './Store';
import User from './User';

export class Cart extends Model {
  public id!: string;
  public name: string;
  public Items: Item[];
  public Users: User[];

  static _init(sequelize: Sequelize): void {
    Cart.init(
      {
        id: {
          type: DataTypes.UUID,
          primaryKey: true,
          allowNull: false,
          defaultValue: () => uuidv4(),
        },
        name: {
          type: DataTypes.STRING,
          allowNull: false,
        },
      },
      {
        sequelize,
        modelName: 'Cart',
      },
    );
  }

  getResponse(store?: Store): CartResponse {
    // Sort items according to store ordering
    const sortedItems: Item[] = [];

    if (store) {
      store.categoryOrder.forEach((categoryId) => {
        this.Items.filter((item) => item.Category.id == categoryId).forEach((item) => sortedItems.push(item));
      });
    } else {
      this.Items.forEach((item) => sortedItems.push(item));
    }

    return {
      id: this.id,
      name: this.name,
      items: sortedItems.map((item) => {
        return {
          id: item.id,
          name: item.name,
          checked: item.CartItem.checked,
        };
      }),
      users: this.Users.map((user) => {
        return {
          id: user.id,
          username: user.username,
          email: user.email,
        };
      }),
    };
  }
}
