import { DataTypes, Model, Sequelize } from 'sequelize';
import { v4 as uuidv4 } from 'uuid';
import { UserResponse } from '../../api/user';

export default class User extends Model {
  public id!: string;
  public username: string;
  public password: string;
  public email: string;

  static _init(sequelize: Sequelize): void {
    User.init(
      {
        id: {
          type: DataTypes.UUID,
          primaryKey: true,
          allowNull: false,
          defaultValue: () => uuidv4(),
        },
        username: {
          type: DataTypes.STRING,
          allowNull: false,
          unique: true
        },
        email: {
          type: DataTypes.STRING,
          allowNull: false,
          unique: true
        },
        password: {
          type: DataTypes.STRING,
          allowNull: false,
        },
      },
      {
        sequelize,
        modelName: 'User',
      }
    );
  }

  getResponse(): UserResponse {
    return {
      id: this.id,
      username: this.username,
      email: this.email,
    };
  }
}