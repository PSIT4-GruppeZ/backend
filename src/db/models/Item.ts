import { DataTypes, Model, Sequelize } from 'sequelize';
import { v4 as uuidv4 } from 'uuid';
import { ItemResponse } from '../../api/item';
import { CartItem } from './CartItem';
import { Category } from './Category';

export class Item extends Model {
  public id!: string;
  public name: string;
  public CartItem: CartItem;
  public Category: Category;

  static _init(sequelize: Sequelize,): void {
    Item.init(
      {
        id: {
          type: DataTypes.UUID,
          primaryKey: true,
          allowNull: false,
          defaultValue: () => uuidv4(),
        },
        name: {
          type: DataTypes.STRING,
          allowNull: false,
          unique: true
        }
      },
      {
        sequelize,
        modelName: 'Item',
      }
    );
  }

  getResponse(): ItemResponse {
    return {
      id: this.id,
      name: this.name,
      category: this.Category.getResponse()
    };
  }
}
