import { DataTypes, Model, Sequelize } from 'sequelize';
import { v4 as uuidv4 } from 'uuid';
import { StoreResponse } from '../../api/store';

export class Store extends Model {
  public id!: string;
  public name: string;
  public categoryOrder: string[];

  static _init(sequelize: Sequelize,): void {
    Store.init(
      {
        id: {
          type: DataTypes.UUID,
          primaryKey: true,
          allowNull: false,
          defaultValue: () => uuidv4(),
        },
        name: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        categoryOrder: {
          type: DataTypes.JSON,
          allowNull: true,
        }
      },
      {
        sequelize,
        modelName: 'Store',
      }
    );
  }

  getResponse(): StoreResponse {
    return {
      id: this.id,
      name: this.name,
      categoryOrder: this.categoryOrder,
    };
  }
}