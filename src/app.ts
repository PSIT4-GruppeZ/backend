import express from 'express';
import api from './api';
import helmet from 'helmet';
import morgan from 'morgan';
import { isTest } from './config';
import yaml from 'yamljs';
import swaggerUi from 'swagger-ui-express';
import path from 'path';

const app = express();

app.use(helmet());
!isTest && app.use(morgan('dev'));
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use('/api', api);
app.use('/swagger', swaggerUi.serve, swaggerUi.setup(yaml.load(path.join(__dirname, './api-definition/current.yaml'))));


export default app;