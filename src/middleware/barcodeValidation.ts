import { Request, Response, NextFunction } from 'express';

export default (req: Request, res: Response, next: NextFunction): Response => {
  const barcode = req.body.barcode;

  let even = 0;
  for (let i = 1; i < 12; i += 2) {
    even += parseInt(barcode[i]);
  }
  even = even * 3;

  let odd = 0;
  for (let i = 0; i < 12; i += 2) {
    odd += parseInt(barcode[i]);
  }
  const checksum = 10 - ((odd + even) % 10);

  if (checksum != parseInt(barcode[12])) {
    return res.status(400).json({ errors: 'Invalid EAN-13 Format' });
  }
  next();
};