import { Request, Response, NextFunction } from 'express';
import { validationResult } from 'express-validator';
import { ValidationResponse } from '../types';

export default (req: Request, res: Response<ValidationResponse>, next: NextFunction): Response => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }
  next();
};