import { Request, Response, NextFunction } from 'express';
import JwtTokenProvider from '../service/JwtTokenProvider';

export default async (req: Request, res: Response, next: NextFunction): Promise<void> => {
  const authHeader = req.headers['authorization'];
  const tokenArr = authHeader.split(' ');
  const token = tokenArr.length == 2 ? tokenArr[1] : '';
  const verified = await JwtTokenProvider.verifyToken(token);
  if (verified) {
    const user = await JwtTokenProvider.extractUser(token);
    if (user) {
      req.user = user;
      next();
      return;
    }
  }
  res.status(403).send();
};
