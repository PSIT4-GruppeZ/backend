import User from '../db/models/User';
import * as bcrypt from 'bcrypt';
import UserRepository from '../db/repository/UserRepository';
import JwtTokenProvider from './JwtTokenProvider';

interface Auth {
  user: User;
  jwt: string;
}

type AuthResponse = Auth | false;

class Authentication {
  private static saltRounds = 10;

  public static async authenticate(usernameOrEmail: string, password: string): Promise<AuthResponse> {
    const user = await UserRepository.findByUsernameOrEmail(usernameOrEmail);
    if (user) {
      const match = await bcrypt.compare(password, user.password);
      if (match) {
        const accessToken = JwtTokenProvider.generateToken(user);
        return { user, jwt: accessToken };
      }
    }
    return false;
  }

  public static hashPassword(password: string): string {
    return bcrypt.hashSync(password, this.saltRounds);
  }
}

export default Authentication;