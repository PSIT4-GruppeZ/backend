import User from '../db/models/User';
import * as jwt from 'jsonwebtoken';
import UserRepository from '../db/repository/UserRepository';

interface JwtDecoded {
  sub: string;
  username: string;
  iat: number;
}

class JwtTokenProvider {
  public static generateToken(user: User): string {
    return jwt.sign({ sub: user.id, username: user.username }, process.env.JWTSECRET);
  }

  public static verifyToken(token: string): Promise<boolean> {
    return new Promise((res) => {
      jwt.verify(token, process.env.JWTSECRET, (e) => {
        res(!e);
      });
    });
  }

  public static verifyAndDecodeToken(token: string): Promise<JwtDecoded> {
    return new Promise((res) => {
      jwt.verify(token, process.env.JWTSECRET, (_e, decoded: JwtDecoded) => {
        res(decoded);
      });
    });
  }

  public static async extractUser(token: string): Promise<User> {
    const data = await this.verifyAndDecodeToken(token);
    if (data && data.sub) {
      const user = await UserRepository.findById(data.sub);
      if (user) {
        return user;
      }
    }
  }
}

export default JwtTokenProvider;
