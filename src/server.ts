import app from './app';
import db from './db/database';
import { AppEnv } from './types';
import config, { initDevData, isTest } from './config';

const ENV = (process.argv[2] === 'DEV' ? 'DEV' : isTest ? 'TEST' : 'PROD') as AppEnv;
const PORT = process.env.port || 3001;

if (!config[ENV].envVars.every((v) => v in process.env)) {
  console.error('\nNot all env variables set - Exiting');
  console.error(`Required vars: ${config[ENV].envVars.join(', ')}\n`);
  process.exit();
}

db(ENV).sync().then(async () => {
  ENV !== 'PROD' && await initDevData();
  app.listen(PORT, () => {
    console.log(`ready on port ${PORT} with env: ${ENV}\n`);
  });
});