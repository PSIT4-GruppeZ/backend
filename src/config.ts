import { Options } from 'sequelize';
import { AppEnv } from './types';
import dotenv from 'dotenv';
import UserRepository from './db/repository/UserRepository';
import CategoryRepository from './db/repository/CategoryRepository';
import ItemRepository from './db/repository/ItemRepository';

dotenv.config();

interface Config {
  db: Options;
  envVars: string[];
}

const devEnvVars = ['JWTSECRET', 'DEV_USERNAME', 'DEV_PW', 'DEV_EMAIL'];
const prodEnvVars = ['DATABASE', 'USERNAME', 'PASSWORD', 'HOST', 'DIALECT', 'JWTSECRET'];

export const isTest = process.env.NODE_ENV === 'test';

export const initDevData = async (): Promise<void> => {
  await UserRepository.create(process.env.DEV_USERNAME, process.env.DEV_EMAIL, process.env.DEV_PW);
  await CategoryRepository.create('Fruit & vegetable');
  await CategoryRepository.create('Fish & Seafood');
  await CategoryRepository.create('Bread');
  await CategoryRepository.create('Meat');
  const drinks = await CategoryRepository.create('Drinks');
  drinks && await ItemRepository.create('Red Bull', drinks.id);
};

export default {
  'DEV': {
    db: {
      'dialect': 'sqlite',
      'storage': 'dev-db.sqlite'
    },
    envVars: devEnvVars
  },
  'PROD': {
    db: {
      'database': process.env.DATABASE,
      'username': process.env.USERNAME,
      'password': process.env.PASSWORD,
      'host': process.env.HOST,
      'dialect': process.env.DIALECT,
    },
    envVars: prodEnvVars
  },
  'TEST': {
    db: {
      'dialect': 'sqlite',
      'storage': 'test-db.sqlite',
      'logging': false
    },
    envVars: devEnvVars
  }
} as { [key in AppEnv]: Config };