import { ValidationError } from 'express-validator';
import User from './db/models/User';
export type AppEnv = 'PROD' | 'DEV' | 'TEST';

export interface ValidationResponse {
  errors: ValidationError[];
}

declare global {
  namespace Express {
    export interface Request {
      user: User
    }
  }
}