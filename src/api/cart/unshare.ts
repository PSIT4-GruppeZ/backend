import { Router, Request, Response } from 'express';
import { CartResponse } from '.';
import CartRepository from '../../db/repository/CartRepository';
import CartUserRepository from '../../db/repository/CartUserRepository';
import UserRepository from '../../db/repository/UserRepository';

export default Router().delete('/:cartid/share/:userid', async (req: Request, res: Response<CartResponse>) => {
  let cart = await CartRepository.findById(req.params.cartid, req.user.id);
  if (!cart) {
    res.status(404).send();
    return;
  }

  const user = await UserRepository.findById(req.params.userid);
  if (!user) {
    res.status(404).send();
    return;
  }

  const affected = await CartUserRepository.delete(cart.id, user.id);
  if (affected > 0) {
    cart = await CartRepository.findById(req.params.cartid, req.user.id);
    res.send(cart.getResponse());
  }
  res.status(404).send();
});