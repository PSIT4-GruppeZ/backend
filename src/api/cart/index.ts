import { Router } from 'express';
import create from './create';
import { body, param } from 'express-validator';
import inputValidation from '../../middleware/inputValidation';
import read from './read';
import del from './delete';
import update from './update';
import addItem from './addItem';
import deleteItem from './deleteItem';
import list from './list';
import updateItem from './updateItem';
import share from './share';
import unshare from './unshare';

export interface CartResponse {
  id: string;
  name: string;
  items: {
    id: string;
    name: string;
    checked: boolean;
  }[];
  users: {
    id: string;
    username: string;
    email: string;
  }[];
}

export default Router()
  .get(
    '/',
    inputValidation,
    list
  )
  .get(
    '/:cartid',
    param('cartid').not().isEmpty(),
    inputValidation,
    read
  )
  .delete(
    '/:cartid',
    param('cartid').not().isEmpty(),
    inputValidation,
    del
  )
  .put(
    '/:cartid',
    param('cartid').not().isEmpty(),
    body('name').not().isEmpty(),
    inputValidation,
    update
  )
  .post(
    '/',
    body('name').not().isEmpty(),
    inputValidation,
    create
  )
  .post(
    '/:cartid/items',
    param('cartid').not().isEmpty(),
    body('id').not().isEmpty(),
    inputValidation,
    addItem
  )
  .delete(
    '/:cartid/items/:itemid',
    param('cartid').not().isEmpty(),
    param('itemid').not().isEmpty(),
    inputValidation,
    deleteItem
  )
  .put(
    '/:cartid/items/:itemid',
    param('cartid').not().isEmpty(),
    param('itemid').not().isEmpty(),
    body('checked').isBoolean(),
    inputValidation,
    updateItem
  )
  .post(
    '/:cartid/share',
    param('cartid').not().isEmpty(),
    body('userid').not().isEmpty(),
    inputValidation,
    share
  )
  .delete(
    '/:cartid/share/:userid',
    param('cartid').not().isEmpty(),
    param('userid').not().isEmpty(),
    inputValidation,
    unshare
  );