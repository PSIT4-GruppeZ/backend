import { Router, Request, Response } from 'express';
import { CartResponse } from './index';
import CartRepository from '../../db/repository/CartRepository';

export default Router().get('/:cartid', async (req: Request, res: Response<CartResponse>) => {
  const cart = await CartRepository.findById(req.params.cartid, req.user.id);
  if (!cart) {
    res.status(404).send();
    return;
  }

  res.send(cart.getResponse());
});