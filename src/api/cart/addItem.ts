import { Router, Request, Response } from 'express';
import { CartResponse } from '.';
import CartItemRepository from '../../db/repository/CartItemRepository';
import CartRepository from '../../db/repository/CartRepository';

interface AddItemRequest {
  id: string;
}

export default Router().post('/:cartid/items', async (req: Request, res: Response<CartResponse>) => {
  const { id } = req.body as AddItemRequest;
  let cart = await CartRepository.findById(req.params.cartid, req.user.id);
  if (!cart) {
    res.status(404).send();
    return;
  }

  await CartItemRepository.create(cart.id, id);
  cart = await CartRepository.findById(cart.id, req.user.id);

  res.send(cart.getResponse());
});