import { Router, Request, Response } from 'express';
import { CartResponse } from './index';
import CartRepository from '../../db/repository/CartRepository';

interface UpdateRequest {
  name: string;
}

export default Router().put('/:cartid', async (req: Request, res: Response<CartResponse>) => {
  let cart = await CartRepository.findById(req.params.cartid, req.user.id);
  if (!cart) {
    res.status(404).send();
    return;
  }

  const { name } = req.body as UpdateRequest;

  cart = await CartRepository.update(cart.id, name, req.user.id);

  res.send(cart.getResponse());
});