import { Router, Request, Response } from 'express';
import { CartResponse } from './index';
import CartRepository from '../../db/repository/CartRepository';

export default Router().delete('/:cartid', async (req: Request, res: Response<CartResponse>) => {
  const affected = await CartRepository.deleteById(req.params.cartid, req.user.id);
  
  if (affected > 0) {
    res.send();
    return;
  }
  res.status(404).send();
});