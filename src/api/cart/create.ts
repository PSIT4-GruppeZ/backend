import { Router, Request, Response } from 'express';
import { CartResponse } from '.';
import CartRepository from '../../db/repository/CartRepository';

interface CreateRequest {
  name: string;
}

export default Router().post('/', async (req: Request, res: Response<CartResponse>) => {
  const { name } = req.body as CreateRequest;

  const cart = await CartRepository.create(name, req.user.id);

  res.send(cart.getResponse());
});