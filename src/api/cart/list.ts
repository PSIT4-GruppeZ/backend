import { Router, Request, Response } from 'express';
import { CartResponse } from '.';
import CartRepository from '../../db/repository/CartRepository';

export default Router().get('/', async (req: Request, res: Response<CartResponse[]>) => {
  const carts = await CartRepository.findAll(req.user.id);

  res.send(carts.map((cart) => cart.getResponse()));
});