import { Router, Request, Response } from 'express';
import { CartResponse } from './index';
import CartRepository from '../../db/repository/CartRepository';
import CartItemRepository from '../../db/repository/CartItemRepository';

export default Router().delete('/:cartid/items/:itemid', async (req: Request, res: Response<CartResponse>) => {
  const affected = await CartItemRepository.delete(req.params.cartid, req.params.itemid);
  
  if (affected > 0) {
    const cart = await CartRepository.findById(req.params.cartid, req.user.id);

    res.send(cart.getResponse());
    return;
  }
  res.status(404).send();
});