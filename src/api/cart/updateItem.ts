import { Router, Request, Response } from 'express';
import { CartResponse } from './index';
import CartItemRepository from '../../db/repository/CartItemRepository';
import CartRepository from '../../db/repository/CartRepository';

interface UpdateItemRequest {
  checked: boolean;
}

export default Router().put('/:cartid/items/:itemid', async (req: Request, res: Response<CartResponse>) => {
  let cart = await CartRepository.findById(req.params.cartid, req.user.id);

  if (cart) {
    const cartItem = await CartItemRepository.find(cart.id, req.params.itemid);
    
    if (cartItem) {
      const { checked } = req.body as UpdateItemRequest;
      await CartItemRepository.update(cartItem.CartId, cartItem.ItemId, checked);
      cart = await CartRepository.findById(cart.id, req.user.id);

      res.send(cart.getResponse());
      return;
    }
  }
  res.status(404).send();
});