import { Router, Request, Response } from 'express';
import { CartResponse } from '.';
import CartRepository from '../../db/repository/CartRepository';
import CartUserRepository from '../../db/repository/CartUserRepository';
import UserRepository from '../../db/repository/UserRepository';

interface ShareCartRequest {
  userid: string;
}

export default Router().post('/:cartid/share', async (req: Request, res: Response<CartResponse>) => {
  let cart = await CartRepository.findById(req.params.cartid, req.user.id);
  if (!cart) {
    res.status(404).send();
    return;
  }

  const { userid } = req.body as ShareCartRequest;
  const user = await UserRepository.findById(userid);
  if (!user) {
    res.status(404).send();
    return;
  }

  await CartUserRepository.create(cart.id, user.id);
  cart = await CartRepository.findById(req.params.cartid, req.user.id);

  res.send(cart.getResponse());
});