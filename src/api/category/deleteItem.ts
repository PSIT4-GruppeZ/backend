import { Router, Request, Response } from 'express';
import { CategoryResponse } from '.';
import CategoryRepository from '../../db/repository/CategoryRepository';

export default Router().delete(
  '/:itemid',
  async (req: Request, res: Response<CategoryResponse>): Promise<void> => {
    const affected = await CategoryRepository.deleteById(req.params.itemid);

    if (affected > 0) {
      res.send();
      return;
    }
    res.status(404).send();
  },
);
