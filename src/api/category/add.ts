import { Router, Request, Response } from 'express';
import { CategoryResponse } from '.';
import CategoryRepository from '../../db/repository/CategoryRepository';

interface AddItemRequest {
  name: string;
}

export default Router().post('/', async (req: Request, res: Response<CategoryResponse>) => {
  const { name } = req.body as AddItemRequest;

  const item = await CategoryRepository.create(name);

  res.send(item.getResponse());
});
