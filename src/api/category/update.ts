import { Router, Request, Response } from 'express';
import { CategoryResponse } from '.';
import CategoryRepository from '../../db/repository/CategoryRepository';

interface UpdateRequest {
  name: string;
}

export default Router().put(
  '/:itemid',
  async (req: Request, res: Response<CategoryResponse>): Promise<void> => {
    let item = await CategoryRepository.findById(req.params.itemid);
    if (!item) {
      res.status(404).send();
      return;
    }

    const { name } = req.body as UpdateRequest;

    item = await CategoryRepository.update(item.id, name);

    res.send(item.getResponse());
  },
);
