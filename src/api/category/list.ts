import { Router, Request, Response } from 'express';
import { CategoryResponse } from '.';
import CategoryRepository from '../../db/repository/CategoryRepository';

export default Router().get('/', async (req: Request, res: Response<CategoryResponse[]>) => {
  const items = await CategoryRepository.findAll();

  res.send(items.map((item) => item.getResponse()));
});
