import { Router } from 'express';
import { body, param } from 'express-validator';
import inputValidation from '../../middleware/inputValidation';
import jwtAuth from '../../middleware/jwtAuth';
import add from './add';
import deleteItem from './deleteItem';
import list from './list';
import update from './update';

export interface CategoryResponse {
  id: string;
  name: string;
}

export default Router()
  .get('/', inputValidation, jwtAuth, list)
  .delete('/:itemid', param('itemid').not().isEmpty(), inputValidation, jwtAuth, deleteItem)
  .put('/:itemid', param('itemid').not().isEmpty(), body('name').not().isEmpty(), inputValidation, jwtAuth, update)
  .post('/', body('name').not().isEmpty(), inputValidation, jwtAuth, add);
