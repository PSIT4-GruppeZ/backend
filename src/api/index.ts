import { Router } from 'express';
import cart from './cart';
import item from './item';
import auth from './auth';
import jwtAuth from '../middleware/jwtAuth';
import { header } from 'express-validator';
import inputValidation from '../middleware/inputValidation';
import category from './category';
import store from './store';
import loyalitycard from './loyalitycard';
import user from './user';

export default Router()
  .use('/store', header('authorization').not().isEmpty(), inputValidation, jwtAuth, store)
  .use('/cart', header('authorization').not().isEmpty(), inputValidation, jwtAuth, cart)
  .use('/item', header('authorization').not().isEmpty(), inputValidation, jwtAuth, item)
  .use('/category', header('authorization').not().isEmpty(), inputValidation, jwtAuth, category)
  .use('/loyalitycard', header('authorization').not().isEmpty(), inputValidation, jwtAuth, loyalitycard)
  .use('/user', header('authorization').not().isEmpty(), inputValidation, jwtAuth, user)
  .use('/auth', auth);
