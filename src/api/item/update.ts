import { Router, Request, Response } from 'express';
import { ItemResponse } from '.';
import ItemRepository from '../../db/repository/ItemRepository';

interface UpdateRequest {
  name: string;
}

export default Router().put('/:itemid', async (req: Request, res: Response<ItemResponse>) => {
  let item = await ItemRepository.findById(req.params.itemid);
  if (!item) {
    res.status(404).send();
    return;
  }

  const { name } = req.body as UpdateRequest;

  item = await ItemRepository.update(item.id, name);

  res.send(item.getResponse());
});