import { Router, Request, Response } from 'express';
import { ItemResponse } from '.';
import ItemRepository from '../../db/repository/ItemRepository';

interface CreateRequest {
  name: string;
  categoryId: string;
}

export default Router().post('/', async (req: Request, res: Response<ItemResponse>) => {
  const { name, categoryId } = req.body as CreateRequest;

  const item = await ItemRepository.create(name, categoryId);
  if (item) {
    res.send(item.getResponse());
  } else {
    res.sendStatus(400);
  }
});