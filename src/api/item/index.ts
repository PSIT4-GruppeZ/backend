import { Router } from 'express';
import { body, param } from 'express-validator';
import inputValidation from '../../middleware/inputValidation';
import create from './create';
import read from './read';
import update from './update';
import deleteItem from './delete';
import list from './list';

export interface ItemResponse {
  id: string;
  name: string;
  category: {
    id: string;
    name: string;
  };
}

export default Router()
  .get(
    '/',
    inputValidation,
    list
  )
  .get(
    '/:query',
    param('query').not().isEmpty(),
    inputValidation,
    read
  )
  .delete(
    '/:itemid',
    param('itemid').not().isEmpty(),
    inputValidation,
    deleteItem
  )
  .put(
    '/:itemid',
    param('itemid').not().isEmpty(),
    body('name').not().isEmpty(),
    inputValidation,
    update
  )
  .post(
    '/',
    body('name').not().isEmpty(),
    body('categoryId').not().isEmpty(),
    inputValidation,
    create
  );