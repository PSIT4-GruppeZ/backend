import { Router, Request, Response } from 'express';
import { ItemResponse } from '.';
import ItemRepository from '../../db/repository/ItemRepository';

export default Router().get('/:query', async (req: Request, res: Response<ItemResponse>) => {
  const item = await ItemRepository.findByIdOrName(req.params.query);
  if (!item) {
    res.status(404).send();
    return;
  }

  res.send(item.getResponse());
});