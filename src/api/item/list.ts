import { Router, Request, Response } from 'express';
import { ItemResponse } from '.';
import ItemRepository from '../../db/repository/ItemRepository';

export default Router().get('/', async (req: Request, res: Response<ItemResponse[]>) => {
  const items = await ItemRepository.findAll();

  res.send(items.map((item) => item.getResponse()));
});