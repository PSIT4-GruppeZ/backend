import { Router, Request, Response } from 'express';
import { ItemResponse } from '.';
import ItemRepository from '../../db/repository/ItemRepository';

export default Router().delete('/:itemid', async (req: Request, res: Response<ItemResponse>) => {
  const affected = await ItemRepository.deleteById(req.params.itemid);
  
  if (affected > 0) {
    res.send();
    return;
  }
  res.status(404).send();
});