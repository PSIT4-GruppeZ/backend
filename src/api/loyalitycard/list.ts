import { Router, Request, Response } from 'express';
import { LoyalityCardResponse } from '.';
import LoyalityCardRepository from '../../db/repository/LoyalityCardRepository';

export default Router().get('/', async (req: Request, res: Response<LoyalityCardResponse[]>) => {
  const loyalitycards = await LoyalityCardRepository.findAll(req.user.id);

  res.send(loyalitycards.map((loyalitycard) => loyalitycard.getResponse()));
});
