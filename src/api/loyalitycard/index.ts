import { Router } from 'express';
import { body, param } from 'express-validator';
import inputValidation from '../../middleware/inputValidation';
import create from './create';
import read from './read';
import update from './update';
import deleteItem from './delete';
import list from './list';
import barcodeValidation from '../../middleware/barcodeValidation';

export interface LoyalityCardResponse {
  id: string;
  name: string;
  barcode: string;
}

export default Router()
  .get(
    '/',
    inputValidation,
    list
  )

  .get(
    '/:itemid',
    param('itemid').not().isEmpty(),
    inputValidation,
    read
  )

  .delete(
    '/:itemid',
    param('itemid').not().isEmpty(),
    inputValidation,
    deleteItem
  )

  .put(
    '/:itemid',
    param('itemid').not().isEmpty(),
    body('name').not().isEmpty(),
    body('barcode').isLength({min: 13, max: 13}),
    barcodeValidation,
    inputValidation,
    update
  )
  
  .post(
    '/',
    body('name').not().isEmpty(),
    body('barcode').isLength({min: 13, max: 13}),
    barcodeValidation,
    inputValidation,
    create
  );
