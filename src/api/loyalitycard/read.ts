import { Router, Request, Response } from 'express';
import { LoyalityCardResponse } from '.';
import LoyalityCardRepository from '../../db/repository/LoyalityCardRepository';

export default Router().get('/:loyalitycardid', async (req: Request, res: Response<LoyalityCardResponse>) => {
  const loyalityCard = await LoyalityCardRepository.findById(req.params.loyalitycardid, req.user.id);
  if (!loyalityCard) {
    res.status(404).send();
    return;
  }

  res.send(loyalityCard.getResponse());
});
