import { Router, Request, Response } from 'express';
import { LoyalityCardResponse } from '.';
import LoyalityCardRepository from '../../db/repository/LoyalityCardRepository';

interface UpdateRequest {
  name: string;
  barcode: string;
}

export default Router().put(
  '/:loyalitycardid',
  async (req: Request, res: Response<LoyalityCardResponse>): Promise<void> => {
    const loyalityCard = await LoyalityCardRepository.findById(req.params.loyalitycardid, req.user.id);
    if (!loyalityCard) {
      res.sendStatus(404);
      return;
    }

    const { name, barcode } = req.body as UpdateRequest;

    res.send(
      (await LoyalityCardRepository.update(req.params.loyalitycardid, name, barcode, req.user.id)).getResponse(),
    );
  },
);
