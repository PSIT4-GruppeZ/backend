import { Router, Request, Response } from 'express';
import { LoyalityCardResponse } from '.';
import LoyalityCardRepository from '../../db/repository/LoyalityCardRepository';

export default Router().delete(
  '/:loyalitycartid',
  async (req: Request, res: Response<LoyalityCardResponse>): Promise<void> => {
    const affected = await LoyalityCardRepository.deleteById(req.params.loyalitycartid, req.user.id);

    if (affected > 0) {
      res.send();
      return;
    }
    res.sendStatus(404);
  },
);
