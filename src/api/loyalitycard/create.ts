import { Router, Request, Response } from 'express';
import { LoyalityCardResponse } from '.';
import LoyalityCardRepository from '../../db/repository/LoyalityCardRepository';

interface CreateRequest {
  name: string;
  barcode: string;
}

export default Router().post('/', async (req: Request, res: Response<LoyalityCardResponse>) => {
  const { name, barcode } = req.body as CreateRequest;

  const item = await LoyalityCardRepository.create(name, barcode, req.user.id);

  res.send(item.getResponse());
});
