import { Router, Request, Response } from 'express';
import { UserResponse } from '.';
import UserRepository from '../../db/repository/UserRepository';

export default Router().get('/:usernameOrEmail', async (req: Request, res: Response<UserResponse>) => {
  const user = await UserRepository.findByUsernameOrEmail(req.params.usernameOrEmail);
  if (!user) {
    res.status(404).send();
    return;
  }

  res.send(user.getResponse());
});