import { Router } from 'express';
import { param } from 'express-validator';
import inputValidation from '../../middleware/inputValidation';
import list from './list';
import read from './read';

export interface UserResponse {
  id: string;
  username: string;
  email: string;
}

export default Router()
  .get(
    '/',
    inputValidation,
    list
  )
  .get(
    '/:usernameOrEmail',
    param('usernameOrEmail').not().isEmpty(),
    inputValidation,
    read
  );