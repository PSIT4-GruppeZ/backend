import { Router } from 'express';
import login from './login';
import signup from './signup';
import { body } from 'express-validator';
import inputValidation from '../../middleware/inputValidation';

export default Router()
  .use(
    '/login',
    body('usernameOrEmail').not().isEmpty(),
    body('password').not().isEmpty(),
    inputValidation,
    login
  )
  .use(
    '/signup',
    body('username').not().isEmpty(),
    body('email').isEmail().normalizeEmail(),
    body('password').isLength({ min: 6 }),
    inputValidation,
    signup
  );