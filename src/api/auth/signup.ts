import { Router, Request, Response } from 'express';
import UserRepository from '../../db/repository/UserRepository';
import { UserResponse } from '../user';

export interface Error {
  message: string;
}

interface SignupRequest {
  username: string;
  email: string;
  password: string;
}

export type SignupResponse = UserResponse | Error;

export default Router().post('/', async (req: Request, res: Response<SignupResponse>) => {
  const { username, email, password } = req.body as SignupRequest;

  const [emailExists, usernameExists] = await Promise.all(
    [UserRepository.existsByEmail(email), UserRepository.existsByUsername(username)]
  );

  if (emailExists) {
    return res.status(400).send({ message: 'Email Address is already in use' });
  }
  if (usernameExists) {
    return res.status(400).send({ message: 'Username is already in use' });
  }

  const user = await UserRepository.create(username, email, password);
  return res.send(user.getResponse());
});