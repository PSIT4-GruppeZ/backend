import { Router, Request, Response } from 'express';
import Authentication from '../../service/Authentication';

export interface LoginResponse {
  token: string;
}

interface LoginRequest {
  usernameOrEmail: string;
  password: string;
}

export default Router().post('/', async (req: Request, res: Response<LoginResponse>) => {
  const { usernameOrEmail, password } = req.body as LoginRequest;
  const auth = await Authentication.authenticate(usernameOrEmail, password);

  if (auth) {
    res.send({ token: auth.jwt });
  } else {
    res.status(403).send();
  }
});