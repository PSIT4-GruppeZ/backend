import { Router, Request, Response } from 'express';
import { StoreResponse } from './index';
import StoreRepository from '../../db/repository/StoreRepository';

export default Router().delete('/:storeid', async (req: Request, res: Response<StoreResponse>) => {
  const affected = await StoreRepository.deleteById(req.params.storeid);
  
  if (affected > 0) {
    res.send();
    return;
  }
  res.status(404).send();
});