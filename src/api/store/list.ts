import { Router, Request, Response } from 'express';
import { StoreResponse } from '.';
import StoreRepository from '../../db/repository/StoreRepository';

export default Router().get('/', async (req: Request, res: Response<StoreResponse[]>) => {
  const stores = await StoreRepository.findAll();

  res.send(stores.map((store) => store.getResponse()));
});
