import { Router } from 'express';
import { body, param } from 'express-validator';
import inputValidation from '../../middleware/inputValidation';
import create from './create';
import read from './read';
import update from './update';
import del from './delete';
import list from './list';
import updateOrder from './updateOrder';

export interface StoreResponse {
  id: string;
  name: string;
  categoryOrder: string[];
}

export default Router()
  .get(
    '/',
    inputValidation,
    list
  )
  .get(
    '/:storeid',
    param('storeid').not().isEmpty(),
    inputValidation,
    read
  )
  .delete(
    '/:storeid',
    param('storeid').not().isEmpty(),
    inputValidation,
    del
  )
  .put(
    '/:storeid',
    param('storeid').not().isEmpty(),
    body('name').not().isEmpty(),
    inputValidation,
    update
  )
  .put(
    '/:storeid/order',
    param('storeid').not().isEmpty(),
    body('categoryOrder').not().isEmpty(),
    inputValidation,
    updateOrder
  )
  .post(
    '/',
    body('name').not().isEmpty(),
    inputValidation,
    create
  );