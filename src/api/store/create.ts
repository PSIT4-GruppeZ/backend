import { Router, Request, Response } from 'express';
import { StoreResponse } from './index';
import StoreRepository from '../../db/repository/StoreRepository';

interface CreateRequest {
  name: string;
}

export default Router().post('/', async (req: Request, res: Response<StoreResponse>) => {
  const { name } = req.body as CreateRequest;

  const store = await StoreRepository.create(name);

  res.send(store.getResponse());
});