import { Router, Request, Response } from 'express';
import { StoreResponse } from './index';
import StoreRepository from '../../db/repository/StoreRepository';

interface UpdateRequest {
  name: string;
}

export default Router().put('/:storeid', async (req: Request, res: Response<StoreResponse>) => {
  let store = await StoreRepository.findById(req.params.storeid);
  if (!store) {
    res.status(404).send();
    return;
  }

  const { name } = req.body as UpdateRequest;

  store = await StoreRepository.update(store.id, name);

  res.send(store.getResponse());
});
