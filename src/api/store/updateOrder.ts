import { Router, Request, Response } from 'express';
import { StoreResponse } from './index';
import StoreRepository from '../../db/repository/StoreRepository';

interface UpdateRequest {
  categoryOrder: string[];
}

export default Router().put('/:storeid/order', async (req: Request, res: Response<StoreResponse>) => {
  let store = await StoreRepository.findById(req.params.storeid);
  if (!store) {
    res.status(404).send();
    return;
  }

  const { categoryOrder } = req.body as UpdateRequest;

  store = await StoreRepository.updateCategoryOrder(store.id, categoryOrder);

  res.send(store.getResponse());
});
