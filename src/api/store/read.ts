import { Router, Request, Response } from 'express';
import { StoreResponse } from './index';
import StoreRepository from '../../db/repository/StoreRepository';

export default Router().get('/:storeid', async (req: Request, res: Response<StoreResponse>) => {
  const store = await StoreRepository.findById(req.params.storeid);
  if (!store) {
    res.status(404).send();
    return;
  }

  res.send(store.getResponse());
});