# Backend for Shopping app

## Available scripts

Install dependencies

    yarn install

Run in watch mode

    yarn watch

Run with attached debugger

    yarn debug

> With VS Code -> <kbd>Ctrl</kbd> + <kbd>Shift</kbd> + <kbd>P</kbd> -> "Debug: Toggle Auto Attach" set to "Only with Flag"

Run the linter

    yarn lint

Fix problems found by the linter

    yarn lint:fix

Run test

    yarn test

To debug tests either use

    yarn test:debug

Or if you are on windows

    yarn test:debug:win

Make sure to install these VS Code Extensions (restart VSC after installation):

- ESLint `dbaeumer.vscode-eslint`
- Prettier - Code formatter `esbenp.prettier-vscode`

For prettier you can activate auto formatting by going to File -> Preferences -> Settings, then search for `Editor: Format On Save` and activate it.

Also set VS Codes indentation to use two spaces (press <kbd>Ctrl</kbd> + <kbd>Shift</kbd> + <kbd>P</kbd> and type `Indent Using Spaces`) otherwise Prettier and ESLint will bitch about it.

Add a .env file in the root of the project with the following entries:

```
JWTSECRET=<jwtsecret>
DEV_USERNAME=<some username>
DEV_PW=<some password>
DEV_EMAIL=<some email>
```

> You can also use the provided `.env.test.dev` file and rename it to .env

## Run in Production mode

Start the app using node (dont use an npm script if you start the app in e.g. a container)

    node /build/server.js

Add a .env file in the root of the project with the following entries:

```
DATABASE=<db name>
USERNAME=<db username>
PASSWORD=<db pw>
HOST=<db host>
DIALECT=<db dialect>
JWTSECRET=<jwtsecret>
```

> You can also use the provided `.env.prod` file and rename it to .env

## Folder structure

- /src (all code goes in here)
  - /api (all REST api controllers and middlewares)
    - index (exports all routes under /api and does the input validation)
    - cart (/api/cart controller)
    - /auth (all auth routes)
      - index (exports all routes under /api/auth and does the input validation)
      - login (/api/auth/login controller)
      - signup (/api/auth/signup controller)
  - /db (database models, config and connection setup)
    - /models (all models in our domain)
      - CartItem (model for the CartItem entity)
    - config (env dependent configuration for the db connection)
    - database (db connection and initialization setup)
  - server (main entry point for the app, establishes the db connection and starts the express server)
  - types (global types)
- /test (jest test suits)
  - /api (all test for our api endpoints)
